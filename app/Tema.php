<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model
{
    protected $fillable = ['nombre', 'descripcion', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
