<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puntuacion extends Model
{
    protected $fillable = ['valor', 'tipo', 'tipo_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    } 
}
