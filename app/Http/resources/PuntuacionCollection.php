<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PuntuacionCollection extends ResourceCollection
{
    public $collects = PuntuacionResouce::class; 

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'links' => [
                'self' => route('puntuacion.index')
            ],
            'meta' => [
                'count' => $this->collection->count()
            ]
        ];
    }
}
