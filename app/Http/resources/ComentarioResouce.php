<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ComentarioResouce extends JsonResource
{
    
    public function toArray($request)
    {
        return [
            'type' => 'comentario',
            'id' => $this->resource->getRouteKey(),
            'attributes' => [
                'cuerpo' => $this->resource->cuerpo,
                'fecha' => $this->resource->created_at,
                'user_id' => $this->resource->user_id
            ],
            'relations' => [
                'user' => [
                    'name' => $this->resource->user->name,
                    'puntos' => '0'
                ]
            ],
            'links' => route('comentario.show', $this->resource->getRouteKey())
        ];
    }
}
