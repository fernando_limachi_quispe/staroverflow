<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ComentarioCollection extends ResourceCollection
{

    public $collects = ComentarioResouce::class; 

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'links' => [
                'self' => route('comentario.index')
            ],
            'meta' => [
                'count' => $this->collection->count()
            ]
        ];
    }
}
