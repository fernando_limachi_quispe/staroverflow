<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PuntuacionResouce extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'puntuacion',
            'id' => $this->resource->getRouteKey(),
            'attributes' => [
                'valor' => $this->resource->valor,
                'user_id' => $this->resource->user_id
            ],
            'relations' => [
                'user' => [
                    'name' => $this->resource->user->name,
                    'puntos' => '0'
                ]
            ],
            'links' => route('puntuacion.show', $this->resource->getRouteKey())
        ];
    }
}
