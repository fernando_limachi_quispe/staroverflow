<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TemaResouce extends JsonResource
{
    public function toArray($request)
    {
        return [
            'type' => 'tema',
            'id' => $this->resource->getRouteKey(),
            'attributes' => [
                'nombre' => $this->resource->nombre,
                'descripcion' => $this->resource->descripcion,
                'fecha' => $this->resource->created_at,
                'user_id' => $this->resource->user_id
            ],
            'relations' => [
                'user' => [
                    'name' => $this->resource->user->name,
                    'puntos' => '0'
                ]
            ],
            'links' => route('tema.show', $this->resource->getRouteKey())
        ];
    }
}
