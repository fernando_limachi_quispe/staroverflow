<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TemaCollection extends ResourceCollection
{
    public $collects = TemaResouce::class; 

    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'links' => [
                'self' => route('tema.index')
            ],
            'meta' => [
                'count' => $this->collection->count()
            ]
        ];
    }
}
