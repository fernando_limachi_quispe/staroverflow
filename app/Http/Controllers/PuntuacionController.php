<?php

namespace App\Http\Controllers;

use App\Http\Resources\PuntuacionCollection;
use App\Http\Resources\PuntuacionResouce;
use App\Puntuacion;
use Illuminate\Http\Request;

class PuntuacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return PuntuacionCollection::make(Puntuacion::all());

    }

    public function store(Request $request)
    {
        $e = $this->validacion($request);
        if(count($e) > 0)
            return response()->json(['error' => $e, 418]);
        return PuntuacionResouce::make(Puntuacion::create($request->all()));
    }

    public function show(Puntuacion $puntuacion)
    {
        return PuntuacionResouce::make($puntuacion);
    }


    public function update(Request $request, Puntuacion $puntuacion)
    {
        return PuntuacionResouce::make($puntuacion->update($request->all()));
        
    }

    public function destroy(Puntuacion $puntuacion)
    {
        $puntuacion->delete();
        return 200;
    }

    private function validacion($request){
        $errores = array();
        if($request->valor === null)
            array_push($errores, ["valor"=>'El campo valor es obligatorio']);
        // dd($request->cuerpo === null);
        if($request->user_id  === null)
            array_push($errores, ["user_id"=>'El campo user_id es obligatorio']);
        if($request->tipo  === null)
            array_push($errores, ["tipo"=>'El campo tipo es obligatorio']);
        if($request->tipo_id  === null)
            array_push($errores, ["tipo_id"=>'El campo tipo_id es obligatorio']);
            return $errores;
    }
}
