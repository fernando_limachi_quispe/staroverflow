<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\SocialProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'handleProviderCallback', 'redirectToProvider']]);
    }

    public function login(){
        $credenciales = request(['email','password']);
        // var_dump($credenciales);
        if(!$token = auth()->attempt($credenciales)){
            return response()->json(['error'=>'Correo o contrasenha invalido'], 400);
        }
        return $this->respondWithToken($token, 200);
    }

    public function register(Request $request){
        $email = User::where('email', $request->email)->get();
        if($email->isEmpty()){
            $user = new User;
            $user->name = $request->nombre;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $salida = $user->save();
            $credenciales = ['email'=>$user->email, 'password'=>$user->password];
            // var_dump($credenciales);
            $token = auth()->attempt(['email'=>$user->email, 'password'=>$request->password]);
            return response()->json(['dato' => $user, 'exito' => $salida, 'token' => $token, 'access_type' => 'bearer', 'expires_in' => auth()->factory()->getTTL() * 60], 200);
        }
        else
            return response()->json(['error'=>'El correo ya fue registrado'], 400);        
    }

    private function respondWithToken($token){
        return response()->json([
            'token' => $token,
            'access_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function logout(){
        auth()->logout();
        return response()->json([
            'msg' =>'Se cerro la sesion con exito'
        ]);
    }
    
    public function refresh(){
        $token =auth()->refresh();
        return $this->respondWithToken($token, 200);
    }
    
    public function me(){
        return response()->json(auth()->user());
    }

}
