<?php

namespace App\Http\Controllers;

use App\Http\Resources\TemaCollection;
use App\Http\Resources\TemaResouce;
use App\Tema;
use Illuminate\Http\Request;

class TemaController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    public function index()
    {
        return TemaCollection::make(Tema::all());
    }

    public function store(Request $request)
    {
        $e = $this->validacion($request);
        if(count($e) > 0)
            return response()->json(['error' => $e, 418]);
        return TemaResouce::make(Tema::create($request->all()));
    }

    public function show(Tema $tema)
    {
        return TemaResouce::make($tema);
        
    }

    public function update(Request $request, Tema $tema)
    {
        return TemaResouce::make($tema->update($request->all()));
    }

    public function destroy(Tema $tema)
    {
        $tema->delete();
        return 200;
    }

    private function validacion($request){
        $errores = array();
        if($request->nombre === null)
            array_push($errores, ["nombre"=>'El campo nombre es obligatorio']);
        // dd($request->cuerpo === null);
        if($request->user_id  === null)
            array_push($errores, ["user_id"=>'El campo user_id es obligatorio']);
        if($request->descripcion  === null)
            array_push($errores, ["descripcion"=>'El campo descripcion es obligatorio']);
        return $errores;
    }
}
