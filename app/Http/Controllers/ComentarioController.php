<?php

namespace App\Http\Controllers;

use App\Comentario;
use App\Http\Resources\ComentarioCollection;
use App\Http\Resources\ComentarioResouce;
use Illuminate\Http\Request;

class ComentarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return ComentarioCollection::make(Comentario::all());
    }

    public function store(Request $request)
    {
        $e = $this->validacion($request);
        if(count($e) > 0)
            return response()->json(['error' => $e, 418]);
        return ComentarioResouce::make(Comentario::create($request->all()));

    }

    public function show(Comentario $comentario)
    {
        return ComentarioResouce::make($comentario);
        
    }

    public function update(Request $request, Comentario $comentario)
    {
        // $comentario->update($request->all());
        return ComentarioResouce::make($comentario->update($request->all()));
    }

    public function destroy(Comentario $comentario)
    {
        $comentario->delete();
        return 200;
    }

    private function validacion($request){
        $errores = array();
        if($request->cuerpo === null)
            array_push($errores, ["cuepo"=>'El campo cuerpo es obligatorio']);
        // dd($request->cuerpo === null);
        if($request->user_id  === null)
            array_push($errores, ["user_id"=>'El campo user_id es obligatorio']);
        if($request->tipo  === null)
            array_push($errores, ["tipo"=>'El campo tipo es obligatorio']);
        if($request->tipo_id  === null)
            array_push($errores, ["tipo_id"=>'El campo tipo_id es obligatorio']);
            return $errores;
    }
}
