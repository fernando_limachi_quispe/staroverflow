<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $fillable = ['cuerpo', 'tipo', 'tipo_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }  
}
